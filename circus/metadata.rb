maintainer       "Ferran Peguroles"
maintainer_email "ferran@pegueroles.com"
license          "Apache 2.0"
description      "Installs oniondigital"
long_description "Installs oniondigital"
version          "1.0.2"

%w{debian ubuntu arch redhat centos fedora scientific}.each do |os|
  supports os
end

