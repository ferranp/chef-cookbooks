
package_list = [
  'build-essential',
  'python-dev',
  'python-pip',
]
for package_name in package_list do
  package package_name do
    action :install
  end
end
