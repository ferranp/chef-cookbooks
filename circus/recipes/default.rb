include_recipe "circus::python"

# Install system dependencies.
package_list = [
  'libevent-dev',
  'libzmq-dev',
]
for package_name in package_list do
  package package_name do
    action :install
  end
end

directory "/etc/circus" do
  owner "root"
  group "root"
  mode "0755"
  recursive true
  action :create
end

directory "/etc/circus/conf.d" do
  owner "root"
  group "root"
  mode "0755"
  recursive true
  action :create
end

template "/etc/circus/circus.ini" do
  owner "root"
  group "root"
  mode "0644"
  source "circus.erb"
  action :create
end

# Install circus sistem wide
execute "pip install circus gevent gevent-socketio gevent_zeromq gevent-websocket greenlet beaker" do
  action :run
end

# Install circushttpd dependencies
execute "pip install circus-web" do
  action :run
end

template "/etc/init/circusd.conf" do
  owner "root"
  group "root"
  mode "0755"
  source "circusd.erb"
  action :create
  notifies :restart, "service[circusd]", :delayed
end

service "circusd" do
    provider Chef::Provider::Service::Upstart
    action [:enable,:start]
end
