# Project user.
default['oniondigital']['project_user']['username'] = 'oniondigital'
default['oniondigital']['project_user']['uid'] = 2010
default['oniondigital']['project_user']['gid'] = 2010
default['oniondigital']['project_user']['home'] = '/home/oniondigital'

# Preview configuration
default['oniondigital']['previews']['root'] = '/home/insight/insight/var/previews'
default['oniondigital']['previews']['url'] = 'http://34.34.34.10/previews'

# Circus Log's configuration
default['oniondigital']['circus']['stderr_log_file'] = '/var/log/circus.log'
default['oniondigital']['circus']['stdout_log_file'] = '/var/log/circus.log'

# Git.
default['oniondigital']['git_host'] = 'github.com'
default['oniondigital']['git_user'] = 'git'
